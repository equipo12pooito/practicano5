package app.ito.poo;
import clases.ito.poo.CuerpoCeleste;
import clases.ito.poo.Ubicacion;

public class MyApp {
	
	static void run() {
		CuerpoCeleste c1 = new CuerpoCeleste("Sol", "Gaseoso");
		c1.addLocalizaciones(new Ubicacion(1363, 8920, "Solsticio de verano", 43256));
		c1.addLocalizaciones(new Ubicacion(6724.22F, 5230.5F, "2 semanas", 325623));
		c1.addLocalizaciones(new Ubicacion(1234, 5678, "6 meses", 235667));
		System.out.println(c1);
		System.out.println(c1.desplazamiento(5000, 4000));
		System.out.println(c1.desplazamiento(1363, 3246));
		System.out.println(c1.desplazamiento(1363, 8920));
		System.out.println(c1.desplazamiento(6724.22F,  5230.5F));
	}

	public static void main(String[] args) {
		run();
	}

}
