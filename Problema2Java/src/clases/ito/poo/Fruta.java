package clases.ito.poo;
import java.util.ArrayList;

public class Fruta {

	private String nombre;
	private float extension;
	private float costoPromedio;
	private float precioVentaProm;
	private ArrayList<Periodo> periodos = new ArrayList<Periodo>();

	//=============================
	
	public Fruta() {
		super();
	}

	public Fruta(String nombre, float extension, float costoPromedio, float precioVentaProm) {
		super();
		this.nombre = nombre;
		this.extension = extension;
		this.costoPromedio = costoPromedio;
		this.precioVentaProm = precioVentaProm;
	}

	//=============================

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public float getExtension() {
		return extension;
	}

	public void setExtension(float extension) {
		this.extension = extension;
	}

	public float getCostoPromedio() {
		return costoPromedio;
	}

	public void setCostoPromedio(float costoPromedio) {
		this.costoPromedio = costoPromedio;
	}

	public float getPrecioVentaProm() {
		return precioVentaProm;
	}

	public void setPrecioVentaProm(float precioVentaProm) {
		this.precioVentaProm = precioVentaProm;
	}

	public ArrayList<Periodo> getPeriodos() {
		return this.periodos;
	}
	
	//=============================

	public void agregarPeriodo(String tiempoCosecha, float cantCosechaxtiempo) {
		Periodo p = new Periodo (tiempoCosecha, cantCosechaxtiempo);
		this.periodos.add(p);
	}
	
	public Periodo devolverPeriodo(int i) {
		Periodo p;
		if (i > this.periodos.size() || i < 0)
			p = new Periodo ("null", 0);
		else 
			p = this.periodos.get(i);
		return p;
	}

	public boolean eliminarPeriodo(int i) {
		boolean e;
		if (i > this.periodos.size() || i < 0)
			e = false;
		else {
			this.periodos.remove(i);
			e = true;
		}
		return e;
	}
	
	public float costoPeriodo(Periodo p) {
		float i = 0;
		if (i > this.periodos.size() || i < 0)
			i = 0;
		else 
			i = p.getCantCosechaxtiempo() * this.costoPromedio;
		return i;
	}

	public float gananciaEstimada(Periodo p) {
		float i = 0;
		if (i > this.periodos.size() || i < 0)
			i = 0;
		else 
			i = (p.getCantCosechaxtiempo() * this.precioVentaProm) - costoPeriodo(p);;
		return i;
	}
	
	
	//=============================

	@Override
	public String toString() {
		return "Fruta: " + nombre + "\nExtension: " + extension + "\nCosto Promedio: " + costoPromedio
				+ "\nPrecio Venta Promedio: " + precioVentaProm + "\nPeriodos: " + periodos;
	}
	
}