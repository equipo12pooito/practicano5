package app.ito.poo;
import clases.ito.poo.Prenda;
import java.time.LocalDate;

public class MyApp {
	
	static void run() {
		Prenda c1 = new Prenda(132, "Lana", 35.50F, "Masculino", "Verano");
		System.out.println(c1);
		
		c1.addLote(1, 5500, LocalDate.now());
		c1.addLote(2, 2400, LocalDate.of(2021, 9, 19));
		System.out.println();
		System.out.println(c1);
		
		System.out.println();
		System.out.println(c1.getLote(2));
		
		System.out.println();
		System.out.println(c1.getLote(1).costoProduccion(c1));
		System.out.println(c1.getLote(2).costoProduccion(c1));
		
		System.out.println();
		System.out.println(c1.getLote(1).montoProduccionxlote(c1));
		System.out.println(c1.getLote(1).montoRecuperacionxpieza(c1));
	}

	public static void main(String[] args) {
		run();
	}

}
