package ito.poo.app;

import java.time.LocalDate;
import ito.poo.clases.CuentaBancaria;

public class MyApp {
	
	static void run() {
		CuentaBancaria CB=new CuentaBancaria(13605,"Armando Banquitos", 50000f,LocalDate.of(2018, 8, 21));
		
		//float c;
		System.out.println(CB);
		System.out.println(CB.retiro(98000f));
		System.out.println(CB);
		System.out.println(CB.deposito(10000f));
		System.out.println(CB);
		/*// 2a operacion
		new CuentaBancaria(13605,"Rosita Asertia", 4800f,null);
		System.out.println(CB);
		System.out.println(CB.deposito(1000f,LocalDate.of(2021, 1, 8)));
		System.out.println(CB);		*/
	}
	
	public static void main(String[] args) {
		run();
	}

}
