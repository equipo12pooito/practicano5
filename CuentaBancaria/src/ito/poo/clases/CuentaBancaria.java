package ito.poo.clases;

import java.time.LocalDate;

public class CuentaBancaria {
	 
	private long numCuenta;
	private String nomCliente;
	private float saldo;
	private LocalDate fechaApertura=null;
	private LocalDate fechaActualizacion=null;
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public CuentaBancaria() {
		super();
	}
	public CuentaBancaria(long numCuenta, String nomCliente, float saldo, LocalDate fechaApertura) {
		super();
		this.numCuenta = numCuenta;
		this.nomCliente = nomCliente;
		this.saldo = saldo;
		this.fechaApertura = fechaApertura;
	}
	

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public long getNumCuenta() {
		return numCuenta;
	}
	public void setNumCuenta(long numCuenta) {
		this.numCuenta = numCuenta;
	}
	public String getNomCliente() {
		return nomCliente;
	}
	public void setNomCliente(String nomCliente) {
		this.nomCliente = nomCliente;
	}
	public float getSaldo() {
		return saldo;
	}
	public void setSaldo(float saldo) {
		this.saldo = saldo;
	}
	public LocalDate getFechaApertura() {
		return fechaApertura;
	}
	public void setFechaApertura(LocalDate fechaApertura) {
		this.fechaApertura = fechaApertura;
	}
	public LocalDate getFechaActualizacion() {
		return fechaApertura;
	}
	public void setFechaActualizacion(LocalDate fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	///in:Parte Practica 5///
	
	public boolean retiro(float cantidad) {
		boolean retiro = false;
		if (cantidad <= this.saldo) {
			retiro=true;
			this.saldo = this.saldo - cantidad;
			this.fechaActualizacion = LocalDate.now();
		}
		return retiro;
	}

	
	public boolean deposito(float cantidad) {
		boolean deposito = false;
		LocalDate c=LocalDate.now();
		if (this.fechaApertura.compareTo(c) < 0) {
			deposito=true;
			this.saldo=this.saldo+cantidad;
			this.fechaActualizacion=LocalDate.now();
		}
		return deposito;
	}
		
	///fi:Parte Practica 5///
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public String toString() {
		return "CuentaBancaria [numCuenta=" + numCuenta + ", nomCliente=" + nomCliente + ", saldo=" + saldo
				+ ", fechaApertura=" + fechaApertura + ", fechaActualizacion=" + fechaActualizacion + "]";
	}
	
}